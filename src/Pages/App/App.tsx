import React from "react";
import "../App/App.css";

import { Main } from "../../componenents/Main/Main";
import { Footer } from "../../componenents/Footer/Footer";
import { Newsletter } from "../../componenents/Newsletter/Newsletter";
import { Header } from "../../componenents/Header/Header";

function App() {
  return (
    <div className="App">
      <Header />
      <Main />
      <Newsletter />
      <Footer />
    </div>
  );
}

export default App;
