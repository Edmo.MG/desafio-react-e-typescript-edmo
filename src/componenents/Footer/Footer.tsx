import React, { useState } from "react";

import Facebook from "../images/facebook-icon.png";
import Instagram from "../images/instagram-icon.png";
import Twitter from "../images/twitter-icon.png";
import Youtube from "../images/youtube-icon.png";
import Linkedin from "../images/linkedin-icon.png";

import { Accordion } from "./utils/Accordion";
import { accordionData } from "./utils/AccordionData";

import styles from "./Footer.module.css";

const Footer = () => {
  return (
    <>
      <footer className={styles["footer"]}>
        <div className={styles["info-wrapper"]}>
          <div>
            <div className={styles["list"]}>
              {accordionData.map(({ title, one, two, three, four }) => (
                <Accordion
                  title={title}
                  one={one}
                  two={two}
                  three={three}
                  four={four}
                />
              ))}
            </div>
          </div>

          <div className={styles["social-media-wrapper"]}>
            <div className={styles["top-social-medias"]}>
              <a href="#">
                <img src={Facebook} alt="Facebook" />
              </a>
              <a href="#">
                <img src={Instagram} alt="Instagram" />
              </a>
              <a href="#">
                <img src={Twitter} alt="Twitter" />
              </a>
              <a href="#">
                <img src={Youtube} alt="Youtube" />
              </a>
              <a href="#">
                <img src={Linkedin} alt="Linkedin" />
              </a>
            </div>
            <span className={styles["span-list"]}>www.loremipsum.com</span>
          </div>
        </div>
        <div className={styles["bottom-wrapper"]}>
          <span className={styles["text-desktop"]}>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor
          </span>
          <span className={styles["text-mobile"]}>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. .
          </span>
          <div className={styles["credit-cards"]}>
            <img
              className={styles["footer-card"]}
              src={require("../images/credit-cards-img.png")}
              alt="Cartões de Crédito"
            />
          </div>
          <div className={styles["text-info"]}>
            <img
              className={styles["card"]}
              src={require("../images/developers-img.png")}
              alt="Desenvolvedores"
            />
          </div>
        </div>
      </footer>
    </>
  );
};

export { Footer };
