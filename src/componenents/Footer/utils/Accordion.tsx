import React, { useState } from "react";
import { render, screen } from "@testing-library/react";

import styles from "./Accordion.module.css";

const Accordion = ({
  title,
  one,
  two,
  three,
  four,
}: {
  title: string;
  one: string;
  two: string;
  three: string;
  four: string;
}) => {
  const [isActive, setIsActive] = useState(true);

  return (
    <div className="accordion-item">
      <div
        className={styles["accordion-title"]}
        onClick={() => setIsActive(!isActive)}
      >
        <div>{title}</div>
        <div className={styles["dropdown"]}>{isActive ? "-" : "+"}</div>
      </div>
      {isActive && (
        <div className={styles["accordion-content"]}>
          <li>{one}</li>
          <li>{two}</li>
          <li>{three}</li>
          <li>{four}</li>
        </div>
      )}
    </div>
  );
};

export { Accordion };
