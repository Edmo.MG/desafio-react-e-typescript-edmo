export const accordionData = [
  {
    title: "Institucional",
    one: "Quem Somos",
    two: "Política de Privacidade",
    three: "Segurança",
    four: "Seja um Revendedor",
  },

  {
    title: "Dúvidas",
    one: "Entrega",
    two: "Pagamento",
    three: "Trocas e Devoluções",
    four: "Dúvidas Frequentes",
  },
  {
    title: "Fale Conosco",
    one: "Atendimento ao Consumidor",
    two: "(11) 4159 9504",
    three: "Atendimento Online",
    four: "(11) 99433-8825",
  },
];
