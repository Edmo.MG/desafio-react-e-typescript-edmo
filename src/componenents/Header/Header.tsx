import React, { useRef } from "react";
import { FaBars, FaTimes } from "react-icons/fa";
import styles from "./Header.module.css";

import M3_Academy_Logo from "../images/m3-logo.png";
import Entrar from "../images/entrar-icon.png";
import Cart from "../images/cart-icon.png";

const Header = () => {
  const navRef = useRef<HTMLButtonElement>(null);

  const showNavbar = () => {
    if (navRef.current) {
      navRef.current.classList.toggle(styles["responsive_nav"]);
    }
  };

  return (
    <header className={styles["header"]}>
      <div className={styles["nav-top-wrapper"]}>
        <button onClick={showNavbar} className={styles["nav-btn"]}>
          <FaBars />
        </button>
        <div className={styles["nav-logo"]}>
          <img src={M3_Academy_Logo} alt="M3 Academy Logo" />
        </div>
        <div className={styles["nav-search"]}>
          <input
            className={styles["nav-input"]}
            type="search"
            placeholder="Buscar..."
          />
        </div>
        <div className={styles["nav-chart"]}>
          <img
            className={styles["nav-chart-entrar"]}
            src={Entrar}
            alt="M3 Academy Logo"
          />
          <img src={Cart} alt="M3 Academy Logo" />
        </div>
      </div>
      <nav className={styles["nav-list"]} ref={navRef}>
        <div className={styles["nav-info"]}></div>
        <a href="/#">CURSOS</a>
        <a href="/#">SAIBA MAIS</a>
        <button onClick={showNavbar} className={styles["nav-btn"]}>
          <FaTimes />
        </button>
      </nav>
    </header>
  );
};

export { Header };
