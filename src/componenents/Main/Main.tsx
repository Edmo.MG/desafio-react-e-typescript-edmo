import React from "react";
import { Route, Routes } from "react-router-dom";

import { Sobre } from "./Pages/Sobre/Sobre";
import { Pagamento } from "./Pages/Pagamento/Pagamento";
import { Entrega } from "./Pages/Entrega/Entrega";
import { Troca } from "./Pages/Troca/Troca";
import { Seguranca } from "./Pages/Seguranca/Seguranca";
import { CustomForm } from "../Main/Pages/Contato/Form/CustomForm";

import { Rotas } from "./utils/Rotas/Rotas";

import Seta from "../images/arrow-point-to-right-icon.png";
import Home from "../images/home-icon.png";

import styles from "./Main_Pages.module.css";
const Main = () => {
  return (
    <>
      <main className={styles["main"]}>
        <div className={styles["top"]}>
          <img src={Home} alt="Inicio" />
          <img src={Seta} alt="Seta à direita" />
          <span>institucional</span>
        </div>
        <h1 className={styles["title"]}>institucional</h1>
        <div className={styles["routes-wrapp"]}>
          <Rotas />
          <Routes>
            <Route path="/sobre" element={<Sobre />}></Route>
            <Route path="/forma-de-pagamento" element={<Pagamento />}></Route>
            <Route path="/entrega" element={<Entrega />}></Route>
            <Route path="/troca-e-devolucao" element={<Troca />}></Route>
            <Route
              path="/seguranca-e-privacidade"
              element={<Seguranca />}
            ></Route>
            <Route path="/contato" element={<CustomForm />}></Route>
          </Routes>
        </div>
      </main>
    </>
  );
};

export { Main };
