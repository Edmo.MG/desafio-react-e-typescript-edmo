import React from "react";

import { Formik, Form, Field, ErrorMessage, FormikHelpers } from "formik";

import FormSchema from "./Schema/FormSchema";

import styles from "./CustomForm.module.css";

interface IFormikValues {
  name: string;
  email: string;
  cpf: string;
  birth: string;
  phone: string;
  instagram: string;
  accept: boolean;
  currentDate: string;
}

const initialValues = {
  name: "",
  email: "",
  cpf: "",
  birth: "",
  phone: "",
  instagram: "",
  accept: false,
  currentDate: "",
};

const CustomForm = () => {
  const formSubmit = (
    values: IFormikValues,
    actions: FormikHelpers<IFormikValues>
  ) => {
    console.log(values);
    actions.setSubmitting(true);
    actions.resetForm();
  };

  return (
    <div className={styles["form-wrapper"]}>
      <h2 className={styles["title"]}>preencha o formulário</h2>
      <Formik
        onSubmit={formSubmit}
        initialValues={initialValues}
        validationSchema={FormSchema}
      >
        {({ errors, touched }) => (
          <Form>
            <div className={styles["form-col"]}>
              <label htmlFor="name">Nome</label>
              <Field
                className={errors.name && touched.name && "invalid"}
                placeholder="Seu nome completo"
                id="name"
                name="name"
              />
              <ErrorMessage
                component="span"
                name="name"
                className={styles["form-invalid"]}
              />
            </div>
            <div className={styles["form-col"]}>
              <label htmlFor="email">E-mail</label>
              <Field
                className={errors.email && touched.email && "invalid"}
                placeholder="Seu e-mail"
                id="email"
                name="email"
              />
              <ErrorMessage
                component="span"
                name="email"
                className={styles["form-invalid"]}
              />
            </div>
            <div className={styles["form-col"]}>
              <label htmlFor="cpf">CPF</label>
              <Field
                className={errors.cpf && touched.cpf && "invalid"}
                placeholder="000 000 000 00"
                id="cpf"
                name="cpf"
              />
              <ErrorMessage
                component="span"
                name="cpf"
                className={styles["form-invalid"]}
              />
            </div>
            <div className={styles["form-col"]}>
              <label htmlFor="birth">Data de Nascimento:</label>
              <Field
                className={errors.birth && touched.birth && "invalid"}
                placeholder="00 . 00 . 0000"
                id="birth"
                name="birth"
              />
              <ErrorMessage
                component="span"
                name="birth"
                className={styles["form-invalid"]}
              />
            </div>
            <div className={styles["form-col"]}>
              <label htmlFor="phone">Telefone:</label>
              <Field
                className={errors.phone && touched.phone && "invalid"}
                placeholder="(+00) 00000 0000"
                id="phone"
                name="phone"
              />
              <ErrorMessage
                component="span"
                name="phone"
                className={styles["form-invalid"]}
              />
            </div>
            <div className={styles["form-col"]}>
              <label htmlFor="instagram">Instagram</label>
              <Field
                className={errors.instagram && touched.instagram && "invalid"}
                placeholder="@seuuser"
                id="instagram"
                name="instagram"
              />
              <ErrorMessage
                component="span"
                name="instagram"
                className={styles["form-invalid"]}
              />
            </div>

            <div className={styles["form-check"]}>
              <label htmlFor="checkbox">
                <u>*Declaro que li e aceito</u>
              </label>
              <Field type="checkbox" name="accept" id="accept" />
              <ErrorMessage
                component="span"
                name="accept"
                className={styles["form-invalid"]}
              />
            </div>

            <button type="submit">CADASTRE-SE</button>
          </Form>
        )}
      </Formik>
    </div>
  );
};

export { CustomForm };
