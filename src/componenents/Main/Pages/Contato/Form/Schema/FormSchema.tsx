import { yupToFormErrors } from "formik";
import * as Yup from "yup";
import "yup-phone";
import parse from "date-fns/parse";
import { cpf as cpfValidator } from "cpf-cnpj-validator";

const today = new Date();

export default Yup.object().shape({
  name: Yup.string()
    .min(3, "Por favor, digite um nome válido.")
    .required("*Campo Obrigatório"),

  email: Yup.string().required("*Campo Obrigatório").email("E-mail inválido"),

  cpf: Yup.string()
    .test("999.999.999.99", "*CPF inválido!", (val = "") =>
      cpfValidator.isValid(val)
    )
    .required("*CPF obrigatório!"),

  birth: Yup.date()
    .transform(function (value, originalValue) {
      if (this.isType(value)) {
        return value;
      }
      const result = parse(originalValue, "dd.MM.yyyy", new Date());
      return result;
    })
    .typeError("*Data inválida!")
    .required("*Campo obrigatório!")
    .min("1000-01-01", "Data muito antiga")
    .max(today, "Você veio do futuro?"),

  phone: Yup.string()
    .phone("BR", true, "Telefone inválido!")
    .required("*Campo Obrigatório"),

  instagram: Yup.string().required("*Campo Obrigatório"),

  accept: Yup.bool().oneOf([true], "*Campo Obrigatório!"),
});
