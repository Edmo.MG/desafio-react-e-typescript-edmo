import React from "react";
import { Link } from "react-router-dom";

import styles from "../../Main_Pages.module.css";

const Rotas = () => {
  return (
    <>
      <ul className={styles["lista"]}>
        <li className={styles["link"]}>
          <Link to="/sobre">Sobre</Link>
        </li>
        <li className={styles["link"]}>
          <Link to="/forma-de-pagamento">Forma de Pagamento</Link>
        </li>
        <li className={styles["link"]}>
          <Link to="/entrega">Entrega</Link>
        </li>
        <li className={styles["link"]}>
          <Link to="/troca-e-devolucao">Troca e Devolução</Link>
        </li>
        <li className={styles["link"]}>
          <Link to="/seguranca-e-privacidade">Segurança e Privacidade</Link>
        </li>
        <li className={styles["link"]}>
          <Link to="/contato">Contato</Link>
        </li>
      </ul>
    </>
  );
};
export { Rotas };
