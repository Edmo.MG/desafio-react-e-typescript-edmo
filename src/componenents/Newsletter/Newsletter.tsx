import React, { useEffect, useState } from "react";

import { Formik, Form, Field, ErrorMessage, FormikHelpers } from "formik";

import btnSchema from "./btnSchema";

import ScrollToTop from "./utils/ScrollUp";
import styles from "./Newsletter.module.css";
import Whatsapp from "../images/whatsapp-icon.png";
import Elipse from "../images/arrow-up.png";

interface IFormikValues {
  email: string;
}

const initialValues = {
  email: "",
};

const Newsletter = () => {
  const btnSubmit = (
    values: IFormikValues,
    actions: FormikHelpers<IFormikValues>
  ) => {
    console.log(values);
    actions.setSubmitting(true);
    actions.resetForm();
  };

  return (
    <>
      <div className={styles["newsletter-container"]}>
        <div className={styles["newsletter-wrapper"]}>
          <div className={styles["newsletter-img"]}>
            <a
              href="https://api.whatsapp.com/send?phone=0000000000&text=Olá. Gostaria de mais informações sobre a M3 Academy."
              target="_blank"
            >
              <img
                className={styles["newsletter_img"]}
                src={Whatsapp}
                alt="Whatsapp"
              />
            </a>
            <ScrollToTop />
          </div>
          <div className={styles["newsletter-text"]}>
            <span className="newsletter-span-text">
              ASSINE NOSSA NEWSLETTER
            </span>
          </div>
          <div className={styles["newsletter-submit"]}>
            <div className={styles["newsletter-input"]}>
              <Formik
                onSubmit={btnSubmit}
                initialValues={initialValues}
                validationSchema={btnSchema}
              >
                {({ errors, touched }) => (
                  <Form className={styles["newsletter-form"]}>
                    <div className={styles["form-col"]}>
                      <Field
                        className={errors.email && touched.email && "invalid"}
                        placeholder="E-mail"
                        id="email"
                        name="email"
                      />
                      <ErrorMessage
                        component="span"
                        name="email"
                        className={styles["form-invalid"]}
                      />
                    </div>
                    <button
                      type="submit"
                      className={styles["newsletter_button"]}
                    >
                      ENVIAR
                    </button>
                  </Form>
                )}
              </Formik>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export { Newsletter };
