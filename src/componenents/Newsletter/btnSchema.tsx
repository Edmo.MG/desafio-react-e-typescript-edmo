import { yupToFormErrors } from "formik";
import * as Yup from "yup";

export default Yup.object().shape({
  email: Yup.string().required("*Campo Obrigatório").email("E-mail inválido"),
});
