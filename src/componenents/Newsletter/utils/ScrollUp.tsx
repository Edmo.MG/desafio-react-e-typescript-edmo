import React, { useState, useEffect } from "react";

import Elipse from "../../images/arrow-up.png";
import styles from "./ScrollUp.module.css";

const ScrollToTop = () => {
  const [showTopBtn, setShowTopBtn] = useState(false);
  useEffect(() => {
    window.addEventListener("scroll", () => {
      if (window.scrollY > 40) {
        setShowTopBtn(true);
      } else {
        setShowTopBtn(false);
      }
    });
  }, []);
  const goToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: "smooth",
    });
  };
  return (
    <div className={styles["top-to-btm"]}>
      <div className={styles["top-to-btm"]}>
        {" "}
        {showTopBtn && (
          <img
            className={styles["icon icon style"]}
            src={Elipse}
            alt="Botão para cima"
            onClick={goToTop}
          />
        )}{" "}
      </div>
    </div>
  );
};
export default ScrollToTop;
